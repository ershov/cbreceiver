import EventEmitter from 'events';

const EVENT_TTL = 1000 * 30;
const CHECK_INTERVAL = 1000 * 5;

export default class EventBuffer extends EventEmitter {
    constructor(){
        super();

        this.events = [];

        setInterval(() => {
            this.events = this.events.filter(e => new Date() - e.datetime > EVENT_TTL);
        }, CHECK_INTERVAL);
    }

    push(data) {
        this.events.push({
            data,
            datetime: new Date()
        });
        this.emit('data', data);
    }
    shift() {
        let e = this.events.shift() || {};
        return e.data;
    }
    get length() {
        return this.events.length;
    }
}