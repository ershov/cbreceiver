'use strict';
var path = require('path');

global.debug = false;
global.appRoot = path.join(__dirname);

// Включаем трансшпиллер
require('babel-register')({
    presets: [
        ['env', {target:{node:'current'}}]
    ]
});

require('./app');