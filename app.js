import express from 'express';
import bodyParser from 'body-parser';
import EventBuffer from './lib/event-buffer';

let app = express();

const PORT = process.env.PORT || 4000;
const LONG_POLLING_INTERVAL = 1000*10;

let buffers = {};

// parse application/json
app.use(bodyParser.json());

app.all('/cb/:app_id/', function (req, res) {
    let app_id = req.params.app_id;
    if (req.body.type === 'confirmation') {
        res.send(app_id);
    } else {
        res.send('ok');

        getBuffer(app_id)
            .push(req.body);
    }

    console.log(`app_id ${req.params.app_id}`);
    console.log(`body: ${JSON.stringify(req.body)}`);
});

app.get('/get/:app_id/', (req, res) => {
    let app_id = req.params.app_id;
    let event_buffer = getBuffer(app_id);

    if (event_buffer.length) {
        res.send([event_buffer.shift()]);
    } else {
        let timeout = setTimeout(() => {
            event_buffer.removeAllListeners('data');
            res.send([]);
        }, LONG_POLLING_INTERVAL);

        event_buffer.on('data', () => {
            res.send([event_buffer.shift()]);
            clearTimeout(timeout);
        });

    }
});

function getBuffer(app_id) {
    return buffers[app_id] = buffers[app_id] || new EventBuffer();
}

app.listen(PORT, () => {
    console.log(`App started at port ${PORT}`);
});