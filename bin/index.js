#!/usr/bin/env node

const SERVER_URL = 'https://cbreciever.herokuapp.com';

let program = require('commander');
let pJSON = require('../package');
let rp = require('request-promise');

program
    .version(pJSON.version)
    .option('--port <port>', 'Port of local stand')
    .option('--path  <path>', 'Path for request')
    .option('--id <id>', 'Application id')
    .parse(process.argv);

if (!program['port']) {
    throw new Error('Param --port is required');
}

if (!program['path']) {
    throw new Error('Param --path is required');
}

if (!program['id']) {
    throw new Error('Param --id is required');
}

let source_url = `${SERVER_URL}/get/${program['id']}/`;
let destination_url = `http://localhost:${program['port']}${program['path']}`;
console.log(`Start polling from ${source_url} to ${destination_url}`);

(function request() {
    rp(source_url)
        .then(data => {
            try {
                data = JSON.parse(data);
            } catch(e) {
                console.error(`Wrong message ${data}`)
            }

            if (data.length) {
                let message = JSON.stringify(data[0]);
                console.log(message);

                rp({
                    uri: destination_url,
                    method: 'POST',
                    data: message
                })
                    .then(() => {
                        console.log('ok');
                    })
                    .catch((e) => {
                        console.warn(`Message wasn't delivered! ${e.message}`);
                    });
            }
            request();
        })
        .catch(() => {
            request();
        });
})();
